/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.panneerat.oxlab2;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class OXLab2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;
    private static boolean resetGame = true;

    public static void main(String[] args) {
        printWelcome();
        while (resetGame) {
            resetTable();
            while (true) {
                printTable();
//            printTurn();
                inputRowCol();

                //เงื่อนไขการชนะ
                if (isWinner()) {
                    printTable();
                    printWinner();
                    break;
                }
                if (isDraw()) {
                    printTable();
                    printDraw();
                    break;
                }
                switchPlayer();
            }
            inputContinue();
        }

    }
//ฟังชั่นเริ่มเกม

    private static void printWelcome() {
        System.out.println("Welcome to OX game");
    }

    //ฟังชั่นตารางox
    private static void printTable() {
        System.out.println("--------------");
        for (int i = 0; i < 3; i++) {
            System.out.print(" | ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println();
            System.out.println("--------------");
        }
    }

//    private static void printTurn() {
//        System.out.println(currentPlayer + " Turn");
//    }
    //ฟังชั่นการอินพุตrowcolเข้าและการใส่ตำแหน่งไม่ซ้ำกัน
    private static void inputRowCol() {
        Scanner s = new Scanner(System.in);
        while (true) {
            System.out.print("player " + currentPlayer + " which row and column do you choose? : ");
            row = s.nextInt() - 1;
            col = s.nextInt() - 1;
            if (isValidMove(row, col)) {
                table[row][col] = currentPlayer;
                return;
            } else {
                System.out.println("Invalid move. Try again.");
            }
        }
    }

    //ฟังชั่นสลับผู้เล่น
    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWinner() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }

        return false;
    }

    private static void printWinner() {
        System.out.println("Player " + currentPlayer + " win! ! !");
    }

    //แนวนอน
    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    //แนวตั้ง
    private static boolean checkCol() {
        for (int j = 0; j < 3; j++) {
            if (table[j][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static void inputContinue() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please input continue (y/n)");
        String reset = s.next().trim().toLowerCase();
        resetGame = reset.equals("y");

    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("It's a draw!");
    }

    private static void resetTable() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    }

    private static boolean isValidMove(int row, int col) {
        return (row >= 0 && row < 3 && col >= 0 && col < 3 && table[row][col] == '-');
    }

}
